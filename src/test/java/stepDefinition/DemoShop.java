package stepDefinition;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class DemoShop {
	
	static WebDriver driver;

	@Given("Open the browser")
	public void open_the_browser() {
	   driver = new ChromeDriver();
	   driver.get("https://demowebshop.tricentis.com/");
	   driver.manage().window().maximize();
	}

	@Then("The homepage is displayed")
	public void the_homepage_is_displayed() {
	   driver.findElement(By.linkText("Log in")).click();
	}

	@When("Enter the valid Email")
	public void enter_the_valid_email() {
	    driver.findElement(By.id("Email")).sendKeys("manzoormehadil@gmail.com");
	}

	@When("Enter the valid Password")
	public void enter_the_valid_password() {
	    driver.findElement(By.id("Password")).sendKeys("Mehek@110");
	}

	@When("Click the Login button")
	public void click_the_login_button() {
	    driver.findElement(By.xpath("//input[@value='Log in']")).click();
	}

	@Then("Home page should be display successfully")
	public void home_page_should_be_display_successfully() {
	   System.out.println("Home page display successfully");
	}

	@Given("User have to login the application")
	public void user_have_to_login_the_application() {
	   System.out.println("----------");
	}

	@When("User have to click the Books button")
	public void user_have_to_click_the_books_button() {
		driver.findElement(By.xpath("//li[@class='inactive']//a[contains(text(),'Books')]")).click();
	}

	@When("Click the sort by option")
	public void click_the_sort_by_option() {
	    driver.findElement(By.id("products-orderby")).click();
	}

	@When("Select high to low option")
	public void select_high_to_low_option() {
	    WebElement filter = driver.findElement(By.id("products-orderby"));
	    Select se = new Select(filter);
	    se.selectByVisibleText("Price: High to Low");
	}

	@When("Click add to cart button")
	public void click_add_to_cart_button() throws InterruptedException {
		 driver.findElement(By.xpath("(//input[@type='button'])[3]")).click();
		    Thread.sleep(2000);
		    driver.findElement(By.xpath("(//input[@type='button'])[5]")).click(); 
	}

	@Then("Product have been added to the shopping cart")
	public void product_have_been_added_to_the_shopping_cart() {
		System.out.println("Product have been added successfully");
	}

	@Given("User have to login application")
	public void user_have_to_login_application() {
		 System.out.println("----------");
	}

	@When("User have to click the Electronics button")
	public void user_have_to_click_the_electronics_button() throws InterruptedException {
		driver.findElement(By.xpath("//li[@class='inactive']//a[contains(text(),'Electronics')]")).click();
		Thread.sleep(2000);
	}

	@When("User have to select the cell phones option")
	public void user_have_to_select_the_cell_phones_option() {
		driver.findElement(By.xpath("//li[@class='inactive']//a[contains(text(),'Cell phones')]")).click();
	}

	@When("Select the product And Click add to cart buton")
	public void select_the_product_and_click_add_to_cart_buton() throws InterruptedException {
		driver.findElement(By.xpath("//h2[@class='product-title']//a[contains(text(),'Smartphone')]")).click();
		Thread.sleep(2000);
		driver.findElement(By.id("add-to-cart-button-43")).click();
	}

	@Then("Display the count of items added to the cart")
	public void display_the_count_of_items_added_to_the_cart() {
		WebElement qty = driver.findElement(By.xpath("//span[@class='cart-qty']"));
		System.out.println("Count of the items:"+qty.getText());
	}

	@When("User have to click the Giftcard button")
	public void user_have_to_click_the_giftcard_button() {
		driver.findElement(By.xpath("//ul[@class='list']//a[contains(text(),'Gift Cards')]")).click();
	}

	@When("Click the dispaly option")
	public void click_the_dispaly_option() {
		driver.findElement(By.id("products-pagesize")).click();
	}

	@When("Select four per page option")
	public void select_four_per_page_option() {
		WebElement page = driver.findElement(By.id("products-pagesize"));
		Select sel1 =new Select(page);
		sel1.selectByVisibleText("4");
	}

	@Then("Display one of the giftcard name and price")
	public void display_one_of_the_giftcard_name_and_price() {
		WebElement gift = driver.findElement(By.xpath("//h2[@class='product-title']//a[contains(text(),'$5 Virtual Gift Card')]"));
		System.out.println("Name and price of the gift:" + gift.getText());
	}

	@When("User have to click the logout button")
	public void user_have_to_click_the_logout_button() {
		 driver.findElement(By.xpath("//a[text()='Log out']")).click();
	}

	@Then("User should navigate the home page return login form home page")
	public void user_should_navigate_the_home_page_return_login_form_home_page() {
		WebElement dis = driver.findElement(By.linkText("Log in"));
		System.out.println(dis.isDisplayed());
	   
	}

	@Then("Close the browser")
	public void close_the_browser() {
		driver.close();
	}

}
