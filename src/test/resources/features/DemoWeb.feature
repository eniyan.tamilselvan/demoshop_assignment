Feature: To validate the functionality of Demo Web Shop

Scenario: Validate the Login functionality
Given Open the browser
Then  The homepage is displayed
When  Enter the valid Email 
And   Enter the valid Password
And   Click the Login button
Then  Home page should be display successfully 

Scenario: Validate the Book fuctionality
Given  User have to login the application
When   User have to click the Books button
And    Click the sort by option 
And    Select high to low option
And    Click add to cart button
Then   Product have been added to the shopping cart

Scenario: Validate the Electronics functionality
Given User have to login application
When  User have to click the Electronics button
And   User have to select the cell phones option
And   Select the product And Click add to cart buton
Then  Display the count of items added to the cart

Scenario: Validate the Giftcard functionality
Given User have to login application
When  User have to click the Giftcard button
And   Click the dispaly option 
And   Select four per page option
Then  Display one of the giftcard name and price

Scenario: Validate the Logout functionality
Given User have to login application
When  User have to click the logout button
Then  User should navigate the home page return login form home page
And   Close the browser
